### A Pluto.jl notebook ###
# v0.19.22

using Markdown
using InteractiveUtils

# This Pluto notebook uses @bind for interactivity. When running this notebook outside of Pluto, the following 'mock version' of @bind gives bound variables a default value (instead of an error).
macro bind(def, element)
    quote
        local iv = try Base.loaded_modules[Base.PkgId(Base.UUID("6e696c72-6542-2067-7265-42206c756150"), "AbstractPlutoDingetjes")].Bonds.initial_value catch; b -> missing; end
        local el = $(esc(element))
        global $(esc(def)) = Core.applicable(Base.get, el) ? Base.get(el) : iv(el)
        el
    end
end

# ╔═╡ 36bd7e34-0648-4719-8612-d275f91b6af7
using Pkg;

# ╔═╡ 8171b692-3ab0-11eb-38d7-8b140e7b03dd
#This activates the environment for this notebook and installs necessary packages.
# using Pkg; Pkg.activate("."); Pkg.instantiate(); using Pluto; Pluto.run()
begin
	Pkg.activate(".")
	Pkg.instantiate()
end

# ╔═╡ 4332e264-3a58-11eb-032d-f361adf14c3e
begin
	using DataFrames;
	using JuMP
	using GLPK
	using CSV;
	using LinearAlgebra
	using StatsPlots
	using PlutoUI
	using Printf
	using Dates
	gr();
end

# ╔═╡ 9451e15f-848c-4f6e-b66c-618c4e1f134a
# If the Pkg.activate() failed, you may need to run this cell to install any of these packages.
# begin
# 	Pkg.add("DataFrames")
# 	Pkg.add("JuMP")
# 	Pkg.add("GLPK")
# 	Pkg.add("CSV")
# 	Pkg.add("LinearAlgebra")
# 	Pkg.add("StatsPlots")
# 	Pkg.add("Plots")
# 	Pkg.add("PlutoUI")
# 	Pkg.add("GR")
# 	Pkg.update()
# end

# ╔═╡ 01498128-3a62-11eb-1511-8d0bb7716a79
#Change the dataDir to the location where you have downloaded the repository files.
begin
	dataDir = pwd()
	# dataDir = "/Users/dpg/work/mu-diet-lp-nz" #Mac/Linux style path
	# dataDir = "D:/Sylvia Riddet Work/Work5/NZ 2020/LP_Analysis_Nov2020" #Windows style path
end;

# ╔═╡ e61c457c-83d9-11eb-3a30-97da23d252ad
#Create a result directory and set copycols = true 
begin
		outDir = joinpath( dataDir, "Results" )
		mkpath(outDir);
		copycols = true;
end;

# ╔═╡ 2defed70-3a62-11eb-09f6-43f4266cec68
begin
	RawReqs = CSV.read(joinpath(dataDir, "NZPersonReq.csv"),DataFrame);
	RawReqsCopy = CSV.read(joinpath(dataDir, "NZPersonReq.csv"),DataFrame); 
	#Adding the copy so we can add nutrient fields with a default set to what is in the file. If I don't use a copy I get an erroneous cyclic reference error
	RawFoods = CSV.read(joinpath(dataDir, "NZFoods.csv"),DataFrame);
	end;

# ╔═╡ 7652ac3c-3a63-11eb-11e3-3b46901568d6
function getDiet(Foods, Reqs, resultsPrefix)
	nutrientsToInclude = names(Reqs)
	FoodsToFit = Foods[:, nutrientsToInclude]
	ReqsToFit = Reqs[:, nutrientsToInclude]
	
	nFoods = size(Foods, 1)
	nCats = size(nutrientsToInclude, 1)
	
	nutritionValues = Matrix(FoodsToFit)
	nutritionReqs = Matrix(ReqsToFit)
	
	minNutrition = nutritionReqs[1, :] #minimum nutrition requirements is first row
	maxNutrition = nutritionReqs[2, :] #max nutrition requirements is second row
	
	m = Model()
	set_optimizer(m,GLPK.Optimizer)
	#Create variable of foods to buy with minimum of zero and max of our constraint
	
		maxFoodAmt = 3 * Foods.Amount
	
	@variable(m, 0 <= toBuy[i=1:nFoods] <= maxFoodAmt[i] )
	#Set up objective function to minimise cost computed as
	#sum of (quantity of food to buy * cost of that food) for all foods in basket
	@objective(m, Min, dot(toBuy, Foods.Cost))
	println("Done")

	#Set up nutrition constraints
	println("Nutrition constraint")
	@constraint(
	    m,
	    minNutCons[j = 1:nCats],
	    sum(nutritionValues[i, j] * toBuy[i] for i = 1:nFoods) >= minNutrition[j],
	)
	@constraint(
	    m,
	    maxNutCons[j = 1:nCats],
	    sum(nutritionValues[i, j] * toBuy[i] for i = 1:nFoods) <= maxNutrition[j],
	)
	println("Done")
	
	
	
	
	#Bespoke nutrient requirements
	#change in serving size groups for Food SubGroup White Flour
	println("Food category group constraint for White Flour...")
		@expression(    m,
		foodCatExp1[i = 1:nFoods; Foods.SubGroup[i] == "White Flour" ],
		toBuy[i] / Foods.Amount[i])
	@constraint(m, foodCatCons1, sum(foodCatExp1) <= 3)
	
	 #change in serving size groups for Food SubGroup White Bread
	println("Food category group constraint for White Bread...")
	@expression(    m,
	foodCatExp2[i = 1:nFoods; Foods.SubGroup[i] == "Bread, white" ],
		toBuy[i] / Foods.Amount[i])
	@constraint(m, foodCatCons2, sum(foodCatExp2) <= 3)

	#change in serving size groups for Food SubGroup Vegetable oil
	println("Food category group constraint for Vegetable oil...")
	@expression(    m,
	foodCatExp3[i = 1:nFoods; Foods.SubGroup[i] == "Vegetable oil" ],
		toBuy[i] / Foods.Amount[i])
	@constraint(m, foodCatCons3, sum(foodCatExp3) <= 3)
		
	#change in serving size groups for Food SubGroup Cabbage
	println("Food category group constraint for Cabbage...")
	@expression(    m,
	foodCatExp4[i = 1:nFoods; Foods.SubGroup[i] == "Cabbage" ],
		toBuy[i] / Foods.Amount[i])
	@constraint(m, foodCatCons4, sum(foodCatExp4) <= 3)
		

		#change in serving size groups for Food SubGroup Margarine
	println("Food category group constraint for Margarine...")
	@expression(    m,
	foodCatExp5[i = 1:nFoods; Foods.SubGroup[i] == "Margarine" ],
		toBuy[i] / Foods.Amount[i])
	@constraint(m, foodCatCons5, sum(foodCatExp5) <= 2)
		println("Done")
		
	
	

	#Solve system
	println("Solving...")
	optimize!(m)
	println("Solver finished with status ", termination_status(m))
	println("Primal status ", primal_status(m))
	println("Dual status ", dual_status(m))
	println("Objective value (Cost of optimal diet in \$): ", objective_value(m))

	###########
	#Assemble output dataframes with solutions
	#Dataframe for storing serving size summary for food groups

	uniqueFoodGroups = unique(Foods.Group) #This just grabs the actual unique names..
	foodGroupServings = DataFrame()
	#Initialize quantities to zero
	for i = 1:length(uniqueFoodGroups)
		sym = Symbol(uniqueFoodGroups[i])
		foodGroupServings[!, sym] = [0]
	end
	
	uniqueFoodSubGroups = unique(Foods.SubGroup) #This just grabs the actual unique names..
	foodSubGroupServings = DataFrame()
	foodSubGroupCost = DataFrame()
	#Initialize quantities to zero
	for i = 1:length(uniqueFoodSubGroups)
		sym = Symbol(uniqueFoodSubGroups[i])
		foodSubGroupServings[!, sym] = [0]
		foodSubGroupCost[!, sym] = [0]
	end
	
	uniqueFoodSources = unique(Foods.Source) #This just grabs the actual unique names..
	foodSourceServings = DataFrame()
	#Initialize quantities to zero
	for i = 1:length(uniqueFoodSources)
		sym = Symbol(uniqueFoodSources[i])
		foodSourceServings[!, sym] = [0]
	end


	#Set up dataframe for holding foods contained in optimum diet
	optimDiet = DataFrame(
		# Code = Int64[],
		Name = String[],
		foodGroup = String[],
		foodSubGroup = String[],
		Quantity = Float64[],
		Servings = Float64[],
		Cost = Float64[],
	) #Always want Name in dataframe.
	for j = 1:nCats
		sym = Symbol(names(FoodsToFit)[j])
		optimDiet[!, sym] = Float64[]
		sym = Symbol(names(FoodsToFit)[j], " / min. daily req.")
		optimDiet[!, sym] = Float64[]
		sym = Symbol(names(FoodsToFit)[j], " / max. daily req.")
		optimDiet[!, sym] = Float64[]
	end

	println("Writing results to file...")

	total = fill(0.0, nCats)

	#Loop over all foods in basket
	for i = 1:length(Foods.Name)
	#But only interested if we actually purchase it
	#Use getvalue because toBuy is a variable in the model m so not directly available.
		if (abs(value(toBuy[i])) > 1.e-12)

	#Sum up totals of each nutrient consumed for optimum diet summary
			for j = 1:nCats
				total[j] += nutritionValues[i, j] * value(toBuy[i])
			end

	#Save information about thisFood in a temporary dataframe.
	#Columns need to be the same as what we set up above for optimDiet dataframe
			thisFood = DataFrame(
				# Code = foodCodes[i],
				Name = Foods.Name[i],
				foodGroup = Foods.Group[i],
				foodSubGroup = Foods.SubGroup[i],
				Quantity = value(toBuy[i]),
				Servings = value(toBuy[i]) / Foods.Amount[i],
				Cost = value(toBuy[i]) * Foods.Cost[i],
			)

	#
			idx = 0
			for j = 1:nCats
				idx = idx + 1
				sym = Symbol(names(FoodsToFit)[j])
				thisFood[!, sym] = [value(toBuy[i]) * nutritionValues[i, idx]]
				sym = Symbol(names(FoodsToFit)[j], " / min. daily req.")
				thisFood[!, sym] = [value(toBuy[i]) * nutritionValues[i, idx] /
									minNutrition[idx] ]
				sym = Symbol(names(FoodsToFit)[j], " / max. daily req.")
				thisFood[!, sym] = [value(toBuy[i]) * nutritionValues[i, idx] /
									maxNutrition[idx] ]
			end

	#Append thisFood information to the overall optimDiet dataframe
			append!(optimDiet, thisFood)

	#For food group serving summaries
	#First get column name (food group that this food belongs to)
			sym = Symbol(Foods.Group[i])
	#Add this serving to the amount consumed where
	#num. of servings=(quantity to buy)/(serving size quantity)
			foodGroupServings[!, sym] += [value(toBuy[i] / Foods.Amount[i])]
			
				#For food sub group serving summaries
	#First get column name (food group that this food belongs to)
			sym = Symbol(Foods.SubGroup[i])
	#Add this serving to the amount consumed where
	#num. of servings=(quantity to buy)/(serving size quantity)
			foodSubGroupServings[!, sym] += [value(toBuy[i] / Foods.Amount[i])]
			foodSubGroupCost[!, sym] += [value(toBuy[i] * Foods.Cost[i])]

		end
	end

	#Build summary
	global idx = 0
	optimDietSummary = DataFrame(nutritionName = String[], sumConsumed = Float64[])
	sym = Symbol("sumConsumed / min. daily req.")
	optimDietSummary[!, sym] = Float64[]
	#Save data to summary
	for j = 1:nCats
		global idx = idx + 1
		thisVal = DataFrame(
			nutritionName = String(names(FoodsToFit)[j]),
			sumConsumed = total[idx],
		)
		thisVal[!, sym] = [total[idx] / minNutrition[idx]]
		append!(optimDietSummary, thisVal)
	end


	#Rearrange food group serving size summaries to a cleaner two column dataframe
	fgdf = DataFrame([[names(foodGroupServings)]; collect.(eachrow(foodGroupServings))], [:foodGroup; :servingSums])
	
	CSV.write(
    joinpath(outDir, resultsPrefix * "_optimDietGroupSummary.csv"),
	fgdf,
	)
	
	#For food sub groups
	fsgdf = DataFrame([[names(foodSubGroupServings)]; collect.(eachrow(foodSubGroupServings)); collect.(eachrow(foodSubGroupCost))], [:foodSubGroup; :servingSums;:Cost])
	CSV.write(
    joinpath(outDir, resultsPrefix * "_optimDietSubGroupSummary.csv"),
fsgdf,
	)
	
	
	
CSV.write(joinpath(outDir, resultsPrefix * "_optimDietFoods.csv"), optimDiet)
CSV.write(
    joinpath(outDir, resultsPrefix * "_optimDietNutrientSummary.csv"),
    optimDietSummary,
)
	
	
	
	
println("Model has duals: ", has_duals(m))

	report = lp_sensitivity_report(m)
#Reduced costs for the food items
# , validCostRange=lp_objective_perturbation_range.(toBuy) #For some reason this isn't working atm
# shadow = DataFrame( Code=foodCodes, Food=foodNames,  shadowPrice=shadow_price.( UpperBoundRef.( toBuy ) ), currentConstraint=maxfoodAmt )
# CSV.write(resultsDir *resultsPrefix * "_shadowPrice.csv", shadow[shadow.shadowPrice .!= 0, :] );
#Sensitivities of the food serving size constraints
foodAmtConsDF = DataFrame(
    # Code = foodCodes,
    Food = Foods.Name,
    foodAmtShadowPrice = shadow_price.(UpperBoundRef.(toBuy)),
    currentConstraint = maxFoodAmt,
    # validConstraintRange = get.(Ref(report), UpperBoundRef.(toBuy)),
)
	foodAmtConsDF.validConstraintRange = missings(Tuple{Float64, Float64}, nrow(foodAmtConsDF))
	for i = 1:nrow(foodAmtConsDF)
		foodAmtConsDF.validConstraintRange[i] = report[UpperBoundRef(toBuy[i])]
	end
CSV.write(
    quotestrings = true,
    joinpath(outDir, resultsPrefix * "_foodAmtShadowPrice.csv"),
    foodAmtConsDF[foodAmtConsDF.foodAmtShadowPrice .!= 0, :]
);
#sensitivities of the minimum nutrition constraints
minNutConsDF = DataFrame(
    nutritionName = String.(names(FoodsToFit)),
    minNutConsShadowPrice = shadow_price.(minNutCons),
    currentConstraint = minNutrition,
    # validConstraintRange = get.(Ref(report),minNutCons),
)
	minNutConsDF.validConstraintRange = missings(Tuple{Float64, Float64}, nrow(minNutConsDF))
	for i = 1:nrow(minNutConsDF)
		minNutConsDF.validConstraintRange[i] = report[minNutCons[i]]
	end
CSV.write(
    quotestrings = true,
    joinpath(outDir, resultsPrefix * "_minNutShadowPrice.csv"),
    minNutConsDF[minNutConsDF.minNutConsShadowPrice.!=0, :],
);
#sensitivities of the maximum nutrition constraints
	
maxNutConsDF = DataFrame(
    nutritionName = String.(names(FoodsToFit)),
    maxNutConsShadowPrice = shadow_price.(maxNutCons),
    currentConstraint = maxNutrition,
    # validConstraintRange = get.(Ref(report),maxNutCons),
)
	maxNutConsDF.validConstraintRange = missings(Tuple{Float64, Float64}, nrow(maxNutConsDF))
	for i = 1:nrow(maxNutConsDF)
		maxNutConsDF.validConstraintRange[i] = report[maxNutCons[i]]
	end
CSV.write(
    quotestrings = true,
    joinpath(outDir, resultsPrefix * "_maxNutShadowPrice.csv"),
    maxNutConsDF[maxNutConsDF.maxNutConsShadowPrice.!=0, :],
		);


	#Returns the food group summary, the diet cost, the optimum diet, and nutrient summary
	cfmt = @sprintf("%.3g",objective_value(m))
	return fgdf, cfmt, optimDiet, optimDietSummary, fsgdf
	
end


# ╔═╡ 8b8988c2-82e4-11eb-39d3-9573fc078b05
#This function sets the scale factor for a given food group to the value provided
function setFoodGroupCostScaleFactor( scaleFactorDF, groupToScale, scaleFactor )
	for i in 1:nrow(scaleFactorDF)
		    if scaleFactorDF.foodGroup[i] == groupToScale
				scaleFactorDF.CostScaleFactor[i] = scaleFactor
		    end
	end
	return scaleFactorDF
end

# ╔═╡ 811c22fa-82e4-11eb-1753-8bfa8b4eb72d
# This function returns the input foods database with the costs scaled by the factor provided for that group
function scaleCostOfGroupByFactor( FoodsDB, groupToScale, scaleFactor )
		#Increase the price of all foods in this group by a factor of 1.1, then 1.2
		# then 1.3, and so on.
		for i in 1:nrow(FoodsDB)
		    if FoodsDB.Group[i] == groupToScale
				FoodsDB.Cost[i] *= scaleFactor
		    end
		end
	return FoodsDB
end
	

# ╔═╡ d538bb58-82e6-11eb-3bd3-dbc277cbdce8
function generateScaledFoodsDatabase( FoodsDB, scaleFactorDF )
		#Copy the original foods database and scale the costs of all the food groups in that database.
			scaledFoods = deepcopy(FoodsDB)
		for i in 1:nrow(scaleFactorDF)
			scaledFoods = scaleCostOfGroupByFactor( scaledFoods, scaleFactorDF.foodGroup[i], scaleFactorDF.CostScaleFactor[i] )
		end	
	return scaledFoods
	md"""**Regenerate foods database using cost scalings**"""
end



# ╔═╡ 799ea3e4-8f7b-11eb-2fc8-d50ca20e4124
enablePlantField = @bind enablePlant CheckBox(default=false);

# ╔═╡ 1bb4846f-377c-43f0-841a-bc13daa94a3b
disableVitaminField = @bind disableVitamin CheckBox(default=false);

# ╔═╡ a2c809c2-3bca-41e3-833a-ffd7438abf56
enableWomanField = @bind enableWoman CheckBox(default=false);

# ╔═╡ c223ce40-43d0-11eb-20f8-79f9181f7d09
#Define the fields which allow us to turn on and off the various food types
begin
	includePlantField = @bind includePlant CheckBox(default=true);
	includeAnimalField = @bind includeAnimal CheckBox(default=true);
	if enablePlant
		includeMixedField = @bind includeMixed CheckBox(default=false);		
	else
		includeMixedField = @bind includeMixed CheckBox(default=true);
	end
   	
end;

# ╔═╡ 1b09cbec-82e7-11eb-0d76-e76520a690f1
#Define the fields which let us adjust the costs of food groups by a scale factor
begin
	if enablePlant # If the "Plant" reference diet is enabled, scale up prices of animal foods so they drop out
		milkField = @bind milkScaleFactor NumberField(0.05:0.05:100, default=2.2)
		dairyField = @bind dairyScaleFactor NumberField(0.05:0.05:100, default=3.95)
		eggsField = @bind eggsScaleFactor NumberField(0.05:0.05:100, default=1.8)
		seafoodField = @bind seafoodScaleFactor NumberField(0.05:0.05:100, default=10.3)
		fishField = @bind fishScaleFactor NumberField(0.05:0.05:100, default=2.3)
		sausagesField = @bind sausagesScaleFactor NumberField(0.05:0.05:100, default=1.05)
		lambField = @bind lambScaleFactor NumberField(0.05:0.05:100, default=1.25)
		chickenField = @bind chickenScaleFactor NumberField(0.05:0.05:100, default=1.95)
		porkField = @bind porkScaleFactor NumberField(0.05:0.05:100, default=1)
		beefField = @bind beefScaleFactor NumberField(0.05:0.05:100, default=1)

	else
		milkField = @bind milkScaleFactor NumberField(0.05:0.05:100, default=1)
		dairyField = @bind dairyScaleFactor NumberField(0.05:0.05:100, default=1)
		eggsField = @bind eggsScaleFactor NumberField(0.05:0.05:100, default=1)
		seafoodField = @bind seafoodScaleFactor NumberField(0.05:0.05:100, default=1)
		fishField = @bind fishScaleFactor NumberField(0.05:0.05:100, default=1)
		sausagesField = @bind sausagesScaleFactor NumberField(0.05:0.05:100, default=1)
		lambField = @bind lambScaleFactor NumberField(0.05:0.05:100, default=1)
		chickenField = @bind chickenScaleFactor NumberField(0.05:0.05:100, default=1)
		porkField = @bind porkScaleFactor NumberField(0.05:0.05:100, default=1)
		beefField = @bind beefScaleFactor NumberField(0.05:0.05:100, default=1)
	end
		
	seedsField = @bind seedsScaleFactor NumberField(0.05:0.05:100, default=1)
	nutsField = @bind nutsScaleFactor NumberField(0.05:0.05:100, default=1)
	fatsField = @bind fatsScaleFactor NumberField(0.05:0.05:100, default=1)
	cerealField = @bind cerealScaleFactor NumberField(0.05:0.05:100, default=1)
	soupField = @bind soupScaleFactor NumberField(0.05:0.05:100, default=1)
	sauceField = @bind sauceScaleFactor NumberField(0.05:0.05:100, default=1)
	sugarField = @bind sugarScaleFactor NumberField(0.05:0.05:100, default=1)
	pastaField = @bind pastaScaleFactor NumberField(0.05:0.05:100, default=1)
	riceField = @bind riceScaleFactor NumberField(0.05:0.05:100, default=1)
	grainField  = @bind grainScaleFactor NumberField(0.05:0.05:100, default=1)
	legField = @bind legScaleFactor NumberField(0.05:0.05:100, default=1)
    vegField = @bind vegScaleFactor NumberField(0.05:0.05:100, default=1)
	fruitField = @bind fruitScaleFactor NumberField(0.05:0.05:100, default=1)
end;



# ╔═╡ 22c77504-2d36-4398-95b9-7be619aa0525
begin
	
#Define the fields which allow us to reactively change the minimum requirements		
# Define our standard requirements for a reference average adult as a starting point
		
		minF1 = @bind minF1v NumberField(0:0.01:500, default=RawReqsCopy.Biotin[1]);
		minF3 = @bind minF3v NumberField(0:0.01:500, default=RawReqsCopy.Chromium[1]);
		minF4 = @bind minF4v NumberField(0:0.01:50, default=RawReqsCopy.Copper[1]);
		minF5 = @bind minF5v NumberField(0:0.1:1000, default=RawReqsCopy[1,Symbol("Dietary folate equivalents")]);
		minF6 = @bind minF6v NumberField(4000:5:50000, 	default=RawReqsCopy[1,Symbol("Energy, total metabolisable (kJ)")]);
		minF7 = @bind minF7v NumberField(0:0.01:50, 		default=RawReqsCopy[1,Symbol("Fatty acid 18:3 omega-3")]);
		minF8 = @bind minF8v NumberField(0:0.01:500, 		default=RawReqsCopy[1,Symbol("Fatty acid cis,cis 18:2 omega-6")]);
		minF9 = @bind minF9v NumberField(0:0.01:500, 		default=RawReqsCopy[1,Symbol("Fibre, total dietary")]);
		minF10 = @bind minF10v NumberField(0:0.01:45, 		default=RawReqsCopy.Iron[1]);
		minF11 = @bind minF11v NumberField(0:0.1:5000, 			default=RawReqsCopy.Magnesium[1]);
		minF12 = @bind minF12v NumberField(0:0.1:50000, 			default=RawReqsCopy.Manganese[1]);
			
		minF14 = @bind minF14v NumberField(0:0.01:35, 		default=RawReqsCopy[1,Symbol("Niacin equivalents, total")]);
		minF15 = @bind minF15v NumberField(0:0.01:50, 			default=RawReqsCopy[1,Symbol("Pantothenic acid")]);
			
		minF17 = @bind minF17v NumberField(0:0.1:50000, 			default=RawReqsCopy.Potassium[1]);
		minF18 = @bind minF18v NumberField(0:0.01:500, 	default=RawReqsCopy[1,Symbol("Protein, total; calculated from total nitrogen")]);
		minF19 = @bind minF19v NumberField(0:0.01:50, 			default=RawReqsCopy.Riboflavin[1]);
		minF20 = @bind minF20v NumberField(0:0.01:500, default=RawReqsCopy.Selenium[1]);
			
		minF22 = @bind minF22v NumberField(0:0.01:50, 			default=RawReqsCopy.Thiamin[1]);
		minF23 = @bind minF23v NumberField(0:0.1:3000, 			default=RawReqsCopy[1,Symbol("Vitamin A, retinol equivalents")]);
		minF24 = @bind minF24v NumberField(0:0.1:50, default=RawReqsCopy[1,Symbol("Vitamin B12")]);	
		minF25 = @bind minF25v NumberField(0:0.01:50, 		default=RawReqsCopy[1,Symbol("Vitamin B6")]);
		minF26 = @bind minF26v NumberField(0:0.1:1000, 			default=RawReqsCopy[1,Symbol("Vitamin C")]); 
		minF27 = @bind minF27v NumberField(0:0.01:80, 		default=RawReqsCopy[1,Symbol("Vitamin D; calculated by summation")]);
		minF28 = @bind minF28v NumberField(0:0.01:300, 			default=RawReqsCopy[1,Symbol("Vitamin E, alpha-tocopherol equivalents")]);
		minF29 = @bind minF29v NumberField(0:0.1:500, 			default=RawReqsCopy[1,Symbol("Vitamin K")]);
		minF30 = @bind minF30v NumberField(0:0.01:40,default=RawReqsCopy.Zinc[1]);
			
			
		minF2 = @bind minF2v NumberField(0:0.1:2500, default=RawReqsCopy.Calcium[1]);
				
		minF13 = @bind minF13v NumberField(0:0.1:2000, 			default=RawReqsCopy.Molybdenum[1]);
			
		minF16 = @bind minF16v NumberField(0:0.1:4000, 			default=RawReqsCopy.Phosphorus[1]);
		
		minF21 = @bind minF21v NumberField(0:0.1:2000, 		default=RawReqsCopy.Sodium[1]);
			
	
	# If we want requirements for a woman then override any standard values now.
	if enableWoman
		minF1 = @bind minF1v NumberField(0:0.01:500,default=25);	
		minF3 = @bind minF3v NumberField(0:0.01:500,default=25);
		minF4 = @bind minF4v NumberField(0:0.01:50,default=1.2);
		minF6 = @bind minF6v NumberField(4000:5:50000, default=9630);
		minF7 = @bind minF7v NumberField(0:0.01:50,default=0.8);
		minF8 = @bind minF8v NumberField(0:0.01:500,default=8);
		minF9 = @bind minF9v NumberField(0:0.01:500,default=25);
		minF10 = @bind minF10v NumberField(0:0.01:45,default=18);
		minF11 = @bind minF11v NumberField(0:0.1:5000,default=315);
		minF12 = @bind minF12v NumberField(0:0.1:50000,default=5000);
		minF14 = @bind minF14v NumberField(0:0.01:35,default=14);
		minF15 = @bind minF15v NumberField(0:0.01:50,default=4);
		minF17 = @bind minF17v NumberField(0:0.1:50000,default=2800);
		minF18 = @bind minF18v NumberField(0:0.01:500,default=46);
		minF19 = @bind minF19v NumberField(0:0.01:50,default=1.1);
		minF20 = @bind minF20v NumberField(0:0.01:500,default=60);
		minF22 = @bind minF22v NumberField(0:0.01:50,default=1.1);
		minF23 = @bind minF23v NumberField(0:0.1:3000,default=700);
		minF28 = @bind minF28v NumberField(0:0.01:300,default=7);
		minF29 = @bind minF29v NumberField(0:0.01:500,default=60);
		minF30 = @bind minF30v NumberField(0:0.01:40,default=8);
	end
	
	# If we are assuming vitamins are supplemented, then override any vitamin requirements by setting defaults to 0
	if disableVitamin
		minF1 = @bind minF1v NumberField(0:0.01:500,default=0);
		minF5 = @bind minF5v NumberField(0:0.1:1000, default=0);	
		minF14 = @bind minF14v NumberField(0:0.01:35,default=0);
		minF15 = @bind minF15v NumberField(0:0.01:50, default=0);
		minF19 = @bind minF19v NumberField(0:0.01:50,default=0);
		minF22 = @bind minF22v NumberField(0:0.01:50,default=0);
		minF23 = @bind minF23v NumberField(0:0.1:3000, default=0);
		minF24 = @bind minF24v NumberField(0:0.1:50,default=0);	
		minF25 = @bind minF25v NumberField(0:0.01:50,default=0);
		minF26 = @bind minF26v NumberField(0:0.1:1000,default=0);
		minF27 = @bind minF27v NumberField(0:0.01:80,default=0);
		minF28 = @bind minF28v NumberField(0:0.01:300,default=0);
		minF29 = @bind minF29v NumberField(0:0.1:500,default=0);
	end
	
			
		#What requirements do we have a minimum set for in the baseline.
		# minSet = Matrix(Reqs)[1,:] .!= 0;
		# reactiveMinNames = names(Reqs[:,minSet])
	
		#Output is:
		
		# "Biotin"
		# "Calcium"
		# "Chromium"
		# "Copper"
		# "Dietary folate equivalents"
		# "Energy, total metabolisable (kJ)"
		# "Fatty acid 18:3 omega-3"
		# "Fatty acid cis,cis 18:2 omega-6"
		# "Fibre, total dietary"
		# "Iron"
		# "Magnesium"
		# "Manganese"
		# "Molybdenum"
		# "Niacin equivalents, total"
		# "Pantothenic acid"
		# "Phosphorus"
		# "Potassium"
		# "Protein, total; calculated from total nitrogen"
		# "Riboflavin"
		# "Selenium"
		# "Sodium"
		# "Thiamin"
		# "Vitamin A, retinol equivalents"
		# "Vitamin B12"
		# "Vitamin B6"
		# "Vitamin C"
		# "Vitamin D; calculated by summation"
		# "Vitamin E, alpha-tocopherol equivalents"
		# "Vitamin K"
		# "Zinc"
	
		
end;	
	
	
	
	


# ╔═╡ 04ff380c-83b0-11eb-09cf-e7b7d482cb9c
begin
	
#Adjust the costs of the various food groups based on the scale factors set by the user

#Create a dataframe with all the foodgroups and their cost scale factor

uniqueFoodGroups = unique(RawFoods.Group)
scaleFactorDF = DataFrame( 
		foodGroup=uniqueFoodGroups,
		CostScaleFactor=Vector{Float64}(undef,length(uniqueFoodGroups)) 
	);

for i in 1:nrow(scaleFactorDF)
	scaleFactorDF.CostScaleFactor[i] = 1.0;
end
#Set the scalefactors based on the values supplied in the value boxes above.
scaleFactorDF = setFoodGroupCostScaleFactor( scaleFactorDF, "Seeds", seedsScaleFactor );

scaleFactorDF = setFoodGroupCostScaleFactor( scaleFactorDF, "Nuts", nutsScaleFactor );

scaleFactorDF = setFoodGroupCostScaleFactor( scaleFactorDF, "Fats and oils", fatsScaleFactor );

scaleFactorDF = setFoodGroupCostScaleFactor( scaleFactorDF, "Breakfast Cereal", cerealScaleFactor );

scaleFactorDF = setFoodGroupCostScaleFactor( scaleFactorDF, "Soup, and Pastry", soupScaleFactor );

scaleFactorDF = setFoodGroupCostScaleFactor( scaleFactorDF, "Sauces, Dressing, Dips and Vinegar", sauceScaleFactor );

scaleFactorDF = setFoodGroupCostScaleFactor( scaleFactorDF, "Sugars and sweets", sugarScaleFactor );

scaleFactorDF = setFoodGroupCostScaleFactor( scaleFactorDF, "Pasta", pastaScaleFactor );

scaleFactorDF = setFoodGroupCostScaleFactor( scaleFactorDF, "Rice", riceScaleFactor );

scaleFactorDF = setFoodGroupCostScaleFactor( scaleFactorDF, "Grain", grainScaleFactor );

scaleFactorDF = setFoodGroupCostScaleFactor( scaleFactorDF, "Legumes", legScaleFactor );

scaleFactorDF = setFoodGroupCostScaleFactor( scaleFactorDF, "Vegetable", vegScaleFactor );

scaleFactorDF = setFoodGroupCostScaleFactor( scaleFactorDF, "Fruit", fruitScaleFactor );

scaleFactorDF = setFoodGroupCostScaleFactor( scaleFactorDF, "Eggs", eggsScaleFactor );

scaleFactorDF = setFoodGroupCostScaleFactor( scaleFactorDF, "Dairy Products", dairyScaleFactor );

scaleFactorDF = setFoodGroupCostScaleFactor( scaleFactorDF, "Milk", milkScaleFactor );

scaleFactorDF = setFoodGroupCostScaleFactor( scaleFactorDF, "Seafood", seafoodScaleFactor );

scaleFactorDF = setFoodGroupCostScaleFactor( scaleFactorDF, "Fish", fishScaleFactor );

scaleFactorDF = setFoodGroupCostScaleFactor( scaleFactorDF, "Sausages, Salami and Ham", sausagesScaleFactor );

scaleFactorDF = setFoodGroupCostScaleFactor( scaleFactorDF, "Lamb", lambScaleFactor );

scaleFactorDF = setFoodGroupCostScaleFactor( scaleFactorDF, "Chicken", chickenScaleFactor );

scaleFactorDF = setFoodGroupCostScaleFactor( scaleFactorDF, "Pork", porkScaleFactor );

scaleFactorDF = setFoodGroupCostScaleFactor( scaleFactorDF, "Beef", beefScaleFactor );

end;

# ╔═╡ f2ff3774-83af-11eb-1638-b599eebaafb0
begin
	#This cell creates the dataframe containing all the reactive nutrient requirements.
	#Unfortunately its not (yet) possible to bind values to elements of an array, so we have to do this manual array version...
	# To change the order simply rearrange the push! lines below.
	mrfDF=DataFrame(
		Nutrients = String[],
		minReqDefault=Float64[],
		minReq=PlutoRunner.Bond[],
	)
	
	push!(mrfDF,[ "Fibre, total dietary (g)", RawReqsCopy[1,Symbol("Fibre, total dietary")], minF9 ])
	push!(mrfDF,[ "Linoleic acid 18:2 omega-6 (g)",
			RawReqsCopy[1,Symbol("Fatty acid cis,cis 18:2 omega-6")], minF8 ])
	push!(mrfDF,[ "alpha-linolenic acid 18:3 omega-3 (g)",
			RawReqsCopy[1,Symbol("Fatty acid 18:3 omega-3")], minF7 ])
	push!(mrfDF,[ "Protein (g)", RawReqsCopy[1,Symbol("Protein, total; calculated from total nitrogen")], minF18 ])

	
	push!(mrfDF,[ "Calcium (mg)", RawReqsCopy.Calcium[1], minF2 ])
	push!(mrfDF,[ "Chromium (ug)", RawReqsCopy.Chromium[1], minF3 ])
	push!(mrfDF,[ "Copper (mg)", RawReqsCopy.Copper[1], minF4 ])
	push!(mrfDF,[ "Iron (mg)", RawReqsCopy.Iron[1], minF10 ])
	push!(mrfDF,[ "Magnesium (mg)", RawReqsCopy.Magnesium[1], minF11 ])
	push!(mrfDF,[ "Manganese (ug)", RawReqsCopy.Manganese[1], minF12 ])
	push!(mrfDF,[ "Molybdenum (ug)", RawReqsCopy.Molybdenum[1], minF13 ])	
	push!(mrfDF,[ "Phosphorus (mg)", RawReqsCopy.Phosphorus[1], minF16 ])
	push!(mrfDF,[ "Potassium (mg)", RawReqsCopy.Potassium[1], minF17 ])
	push!(mrfDF,[ "Selenium (ug)", RawReqsCopy.Selenium[1], minF20 ])
	push!(mrfDF,[ "Sodium (mg)", RawReqsCopy.Sodium[1], minF21 ])
	push!(mrfDF,[ "Zinc (mg)", RawReqsCopy.Zinc[1], minF30 ])

	
	push!(mrfDF,[ "Vitamin A, retinol equivalents (mg)", RawReqsCopy[1,Symbol("Vitamin A, retinol equivalents")], minF23 ])
	push!(mrfDF,[ "Biotin (ug)", RawReqsCopy.Biotin[1], minF1 ])
	push!(mrfDF,[ "Thiamin (mg)", RawReqsCopy.Thiamin[1], minF22 ])
	push!(mrfDF,[ "Riboflavin (mg)", RawReqsCopy.Riboflavin[1], minF19 ])
	push!(mrfDF,[ "Niacin equivalents, total (mg)", RawReqsCopy[1,Symbol("Niacin equivalents, total")], minF14 ])
	push!(mrfDF,[ "Pantothenic acid (mg)", RawReqsCopy[1,Symbol("Pantothenic acid")], minF15 ])
	push!(mrfDF,[ "Dietary folate equivalent (ug)", 
		RawReqsCopy[1,Symbol("Dietary folate equivalents")], minF5 ])
	push!(mrfDF,[ "Vitamin B-6 (mg)", RawReqsCopy[1,Symbol("Vitamin B6")], minF25 ])
	push!(mrfDF,[ "Vitamin B-12 (ug)", RawReqsCopy[1,Symbol("Vitamin B12")], minF24 ])
	push!(mrfDF,[ "Vitamin C (mg)", RawReqsCopy[1,Symbol("Vitamin C")], minF26 ])
	push!(mrfDF,[ "Vitamin D (ug), calculated by summation", RawReqsCopy[1,Symbol("Vitamin D; calculated by summation")], minF27 ])
	push!(mrfDF,[ "Vitamin E (mg), alpha-tocopherol equivalents", RawReqsCopy[1,Symbol("Vitamin E, alpha-tocopherol equivalents")], minF28 ])
	push!(mrfDF,[ "Vitamin K (ug)", RawReqsCopy[1,Symbol("Vitamin K")], minF29 ])

	
	
	# minReqFieldsDF=DataFrame( Nutrients=[ 
	# 		"Biotin",
	# 		"Calcium",
	# 		"Chromium",
	# 		"Copper",
	# 		"Dietary folate equivalents",
	# 		"Fatty acid 18:3 omega-3",
	# 		"Fatty acid cis,cis 18:2 omega-6",
	# 		"Fibre, total dietary",
	# 		"Iron",
	# 		"Magnesium",
	# 		"Manganese",
	# 		"Molybdenum",
	# 		"Niacin equivalents, total",
	# 		"Pantothenic acid",
	# 		"Phosphorus",
	# 		"Potassium",
	# 		"Protein, total; calculated from total nitrogen",
	# 		"Riboflavin",
	# 		"Selenium",
	# 		"Sodium",
	# 		"Thiamin",
	# 		"Vitamin A, retinol equivalents",
	# 		"Vitamin B12",
	# 		"Vitamin B6",
	# 		"Vitamin C",
	# 		"Vitamin D; calculated by summation",
	# 		"Vitamin E, alpha-tocopherol equivalents",
	# 		"Vitamin K",
	# 		"Zinc"], 
	# 	minReqDefault=[
	# 		RawReqsCopy.Biotin[1],
	# 		RawReqsCopy.Calcium[1],
	# 		RawReqsCopy.Chromium[1],
	# 		RawReqsCopy.Copper[1],
	# 		RawReqsCopy[1,Symbol("Dietary folate equivalents")],
	# 		RawReqsCopy[1,Symbol("Fatty acid 18:3 omega-3")],
	# 		RawReqsCopy[1,Symbol("Fatty acid cis,cis 18:2 omega-6")],
	# 		RawReqsCopy[1,Symbol("Fibre, total dietary")],
	# 		RawReqsCopy.Iron[1],
	# 		RawReqsCopy.Magnesium[1],
	# 		RawReqsCopy.Manganese[1],
	# 		RawReqsCopy.Molybdenum[1],
	# 		RawReqsCopy[1,Symbol("Niacin equivalents, total")],
	# 		RawReqsCopy[1,Symbol("Pantothenic acid")],
	# 		RawReqsCopy.Phosphorus[1],
	# 		RawReqsCopy.Potassium[1],
	# 		RawReqsCopy[1,Symbol("Protein, total; calculated from total nitrogen")],
	# 		RawReqsCopy.Riboflavin[1],
	# 		RawReqsCopy.Selenium[1],
	# 		RawReqsCopy.Sodium[1],
	# 		RawReqsCopy.Thiamin[1],
	# 		RawReqsCopy[1,Symbol("Vitamin A, retinol equivalents")],
	# 		RawReqsCopy[1,Symbol("Vitamin B12")],
	# 		RawReqsCopy[1,Symbol("Vitamin B6")],
	# 		RawReqsCopy[1,Symbol("Vitamin C")],
	# 		RawReqsCopy[1,Symbol("Vitamin D; calculated by summation")],
	# 		RawReqsCopy[1,Symbol("Vitamin E, alpha-tocopherol equivalents")],
	# 		RawReqsCopy[1,Symbol("Vitamin K")],
	# 		RawReqsCopy.Zinc[1],
	# 			],
	# 	minReq=[
	# 		minF1,
	# 		minF2,
	# 		minF3,
	# 		minF4,
	# 		minF5,
	# 		minF7,
	# 		minF8,
	# 		minF9,
	# 		minF10,
	# 		minF11,
	# 		minF12,
	# 		minF13,
	# 		minF14,
	# 		minF15,
	# 		minF16,
	# 		minF17,
	# 		minF18,
	# 		minF19,
	# 		minF20,
	# 		minF21,
	# 		minF22,
	# 		minF23,
	# 		minF24,
	# 		minF25,
	# 		minF26,
	# 		minF27,
	# 		minF28,
	# 		minF29,
	# 		minF30]);
end;

# ╔═╡ 1c889c64-83da-11eb-15b7-7fcd7bf7a98e
begin
	selectScaleFactorDF=DataFrame(
		foodGroup = String[],
		CostScaleFactor=PlutoRunner.Bond[],
	)

	#Plant foods
	push!(selectScaleFactorDF,[ "Fruit", fruitField ])
	push!(selectScaleFactorDF,[ "Vegetable", vegField ])
	push!(selectScaleFactorDF,[ "Legumes", legField ])
	push!(selectScaleFactorDF,[ "Grain", grainField ])
	push!(selectScaleFactorDF,[ "Rice", riceField ])
	push!(selectScaleFactorDF,[ "Pasta", pastaField ])
	push!(selectScaleFactorDF,[ "Sugars and sweets", sugarField ])
	push!(selectScaleFactorDF,[ "Sauces, Dressing, Dips and Vinegar", sauceField ])
	push!(selectScaleFactorDF,[ "Soup, and Pastry", soupField ])
	push!(selectScaleFactorDF,[ "Breakfast Cereal", cerealField ])
	push!(selectScaleFactorDF,[ "Fats and oils", fatsField ])
	push!(selectScaleFactorDF,[ "Nuts", nutsField ])
	push!(selectScaleFactorDF,[ "Seeds", seedsField ])

	#Animal foods
	push!(selectScaleFactorDF,[ "Beef", beefField ])
	push!(selectScaleFactorDF,[ "Pork", porkField ])
	push!(selectScaleFactorDF,[ "Chicken", chickenField ])
	push!(selectScaleFactorDF,[ "Lamb", lambField ])
	push!(selectScaleFactorDF,[ "Sausages, Salami and Ham", sausagesField ])
	push!(selectScaleFactorDF,[ "Fish", fishField ])
	push!(selectScaleFactorDF,[ "Seafood", seafoodField ])
	push!(selectScaleFactorDF,[ "Milk", milkField ])
	push!(selectScaleFactorDF,[ "Dairy Products", dairyField ])
	push!(selectScaleFactorDF,[ "Eggs", eggsField ])
	
end;

# ╔═╡ d98a0e30-8d47-11eb-173e-1d1378892115
md"""
---
> Beware that changing the Linear Programming (LP) model parameters (e.g. inclusion of food types, daily energy and nutrient requirements, and/or using cost scale factors) may result in an error message, indicating that a LP cost-minimized optimized diet solution may not be feasible using those settings. 
---
"""


# ╔═╡ 3968203a-ed29-4a4c-b02f-a6b4eb2a667d
md"""
---
> Note that changing most of the settings may affect both the baseline and scaled modelled diets. 
---
"""

# ╔═╡ 15772110-8cff-11eb-0b09-8d0869183986
	md"""
> Choose whether animal, plant, and/or mixed foods are included in the LP analysis.  
"""

# ╔═╡ db083cb6-43d0-11eb-3bb6-114b750ca17b
 
SelectFoodDF = DataFrame( FoodType=["Animal","Plant","Mixed"],Included=[includeAnimalField,includePlantField,includeMixedField] )


# ╔═╡ 51b39790-8f97-11eb-1114-c7c0854527ff
md"""
---

> Enable or Disable the \"Plant\" dietary scenario to include only plant-based foods in the modelled diet. This involved the exclusion of \"Mixed\" foods, and application of cost scale factors to the baseline price of milk by 2.2x, seafood by 10.3x, eggs by 1.8x, dairy products by 3.95x, chicken by 1.95x, fish by 2.3x, lamb by 1.25x, and sausages by 1.05 times. Note that enabling this setting will only affect the scaled diet. 


**Enable \"Plant\" dietary scenario?** $(enablePlantField)"""

# ╔═╡ 2be2d2b3-1cbd-498e-ba21-8456cebed7ee
md"""
---

> Enable or Disable the "No-Vitamin" dietary scenario, whereby the daily requirements for all twelve vitamins may be fulfilled by dietary supplements. 

**Enable "No-Vitamin" dietary scenario?** $(disableVitaminField)"""

# ╔═╡ dfbc49b0-aab9-4041-bea7-4da3d91b0261
md"""
---

> Enable or Disable the dietary scenario based on the daily energy (9630 kJ, 2304 kcal) and nutrient requirements for young female adults. 

**Use daily energy and nutrient requirements for "Females"?** $(enableWomanField)"""

# ╔═╡ 767859cc-891a-11eb-2d85-6fe878eeac9f
md"""
---

> The constraint for the daily estimated energy requirement for a reference adult was set to be met at exactly 11150 kJ (2665 kcal, using a factor of 0.239 kcal to 1 kJ) and may be altered. 

**Energy (kJ)** $(minF6)"""

# ╔═╡ 75c26eb4-83bb-11eb-03df-9b5676f9be43
md"""
---
> The minimum nutrient requirements per day for a reference adult have been given as default values and may be adjusted. Some nutrient requirements will be limited to a maximum amount per day. 
"""

# ╔═╡ 82dfda0a-83b1-11eb-319a-3385adad41ca
mrfDF[1:4,:]

# ╔═╡ 8ab4f6b6-83b1-11eb-2d8d-15a56d5e1374
mrfDF[5:16,:]

# ╔═╡ 933519c2-83b1-11eb-14cc-f7e11f800af5
mrfDF[17:29,:]

# ╔═╡ 2c7c833c-83b3-11eb-2ac1-05ec7d477519
 md"""
---

> The \"Baseline\" diet refers to the least-cost dietary pattern resulting from the baseline prevailing food prices found in the foods database spreadsheet.

> The \"Scaled\" diet refers to the least-cost dietary pattern after each \"CostScaleFactor\" has been applied to the baseline food prices.

> The cost scale factors for each food group involve an increase or decrease, incrementally in 5 % steps, in the baseline prevailing food prices found in the foods database. 

"""

# ╔═╡ 831978b4-83b3-11eb-1eaf-d346ae4503c0
selectScaleFactorDF[1:13,:]

# ╔═╡ a89c6e60-9977-4e36-afa8-3dc999ea6afd
selectScaleFactorDF[14:23,:]

# ╔═╡ 9c0560b8-83b1-11eb-0ed6-852d676a6dc8

begin
	run = @bind runbox CheckBox()
	 runNameField = @bind runName TextField(default="Run1")
		md"""
	---
	
	> Enter run name to identify all results output.

	**Run name:** $(runNameField)
	
	> Check the following box only when you are ready to start the LP analysis. Please uncheck the box while you adjust any LP analysis settings or change any dynamic constraints.
	
	**Enable/disable LP analysis:** $(run)
	
	---
	"""
end

# ╔═╡ c02e8a6a-43d2-11eb-1069-7d35890c9bb8
begin
# begin
	if runbox 
	Reqs = deepcopy(RawReqs)
	
	#Pull in the dynamic min requirement values
	Reqs.Biotin[1] = minF1v
	Reqs.Calcium[1] = minF2v
	Reqs.Chromium[1] = minF3v
	Reqs.Copper[1] = minF4v
	Reqs[1,Symbol("Dietary folate equivalents")] = minF5v
	Reqs[1,Symbol("Energy, total metabolisable (kJ)")] = minF6v
	Reqs[2,Symbol("Energy, total metabolisable (kJ)")] = minF6v
	Reqs[1,Symbol("Fatty acid 18:3 omega-3")] = minF7v
	Reqs[1,Symbol("Fatty acid cis,cis 18:2 omega-6")] = minF8v
	Reqs[1,Symbol("Fibre, total dietary")] = minF9v
	Reqs.Iron[1] = minF10v
	Reqs.Magnesium[1] = minF11v
	Reqs.Manganese[1] = minF12v
	Reqs.Molybdenum[1] = minF13v
	Reqs[1,Symbol("Niacin equivalents, total")] = minF14v
	Reqs[1,Symbol("Pantothenic acid")] = minF15v
	Reqs.Phosphorus[1] = minF16v
	Reqs.Potassium[1] = minF17v
	Reqs[1,Symbol("Protein, total; calculated from total nitrogen")] = minF18v
	Reqs.Riboflavin[1] = minF19v
	Reqs.Selenium[1] = minF20v
	Reqs.Sodium[1] = minF21v
	Reqs.Thiamin[1] = minF22v
	Reqs[1,Symbol("Vitamin A, retinol equivalents")] = minF23v
	Reqs[1,Symbol("Vitamin B12")] = minF24v
	Reqs[1,Symbol("Vitamin B6")] = minF25v
	Reqs[1,Symbol("Vitamin C")] = minF26v
	Reqs[1,Symbol("Vitamin D; calculated by summation")] = minF27v
	Reqs[1,Symbol("Vitamin E, alpha-tocopherol equivalents")] = minF28v
	Reqs[1,Symbol("Vitamin K")] = minF29v
	Reqs.Zinc[1] = minF30v
		
		
	#Apply our food type selection
	Foods = deepcopy(RawFoods);
	if !includeAnimal
			Foods = Foods[Foods.Source .!= "Animal",:];
	end
	if !includePlant
			Foods = Foods[Foods.Source .!= "Plant",:];
	end
		if !includeMixed
			Foods = Foods[Foods.Source .!= "Mixed",:];
	end

	#Get and store the baseline diet results with no modifications to food costs.
	fg, c, od, ods, odsg = getDiet(Foods, Reqs, runName * "_baseline");
	
	#Now compute the foods database with food costs scaled based on the food group cost scale factors
	scaledFoods = generateScaledFoodsDatabase( Foods, scaleFactorDF );
		
		
	#Get the new diet results using the scaled food groups
	newfg, newc, newod, newods, newodsg = getDiet(scaledFoods, Reqs, runName * "_scaled");
	t = Dates.format(now(), "HH:MM:SS") 
	md"""
	LP Analysis completed at $(t).
	---
		
	> LP Analysis enabled. Results plots generated below and saved to output directory.
		

	**Output directory:** $(outDir)
		
	---
		
	"""

	#Plot the baseline diet serving summaries. 
		else
		md"""
		---
		
		> LP Analysis disabled. Please check the box to enable LP analysis and view the results plots.
		
		---
		"""
	end

end

# ╔═╡ 243f99a2-83d6-11eb-094c-0757557186a2
# @df scaleFactorDF bar( :foodGroup,:scaleFactor,legend=false,automargin=true,xlabel="scaleFactor",orientation = :horizontal)
# scaledFoods

# ╔═╡ 83c702c4-8387-11eb-27b0-8b80b54991fc
#Plot the baseline vs scaled diet subgroup summaries.
if runbox
	combinedodsg = DataFrame( foodSubGroup=[odsg.foodSubGroup;newodsg.foodSubGroup], servingSums=[odsg.servingSums;newodsg.servingSums], Cost=[odsg.Cost;newodsg.Cost], ctg=repeat(["Baseline", "Scaled"], inner = size(odsg,1)) )
	combinedodsg = combinedodsg[combinedodsg.servingSums .!= 0,:]
combinedodsg = sort!(combinedodsg,[:Cost,:foodSubGroup,])
subgroupsCombinedPlot = @df combinedodsg groupedbar( :foodSubGroup,:servingSums,group=:ctg,legend=:outerright,automargin=true,ylabel="Servings",xlabel="Food subgroups",orientation = :vertical,xrotation=45,title="Baseline diet NZD" * c*" vs. scaled diet NZD" * newc)
end

# ╔═╡ badc7782-838a-11eb-3a87-1d940979a158
#Plot the baseline vs scaled diet subgroup summaries.
if runbox
subgroupsCostCombinedPlot = @df combinedodsg groupedbar( :foodSubGroup,:Cost,group=:ctg,legend=:outerright,automargin=true,ylabel="Cost (NZD)",xlabel="Food subgroups",orientation = :vertical,xrotation=45,title="Baseline diet NZD" * c*" vs. scaled diet NZD" * newc)
end

# ╔═╡ f9ec5e98-3aaa-11eb-1ca6-81851af40f24
#Plot the baseline vs scaled diet serving summaries.
if runbox
ctg = repeat(["Baseline", "Scaled"], inner = size(fg,1))
combinedfg = DataFrame( foodGroup=[fg.foodGroup;newfg.foodGroup], servingSums=[fg.servingSums;newfg.servingSums] )
groupsCombinedPlot = @df combinedfg groupedbar( :foodGroup,:servingSums,group=ctg,legend=:outerright,automargin=true,xlabel="Food groups",ylabel="Servings",orientation = :vertical,xrotation=45,title="Baseline diet NZD" * c*" vs. scaled diet NZD" * newc)
end

# ╔═╡ 48a88a48-82dd-11eb-3f7e-0da488f56434
#Save all the plots to files
if runbox
	savefig(groupsCombinedPlot,joinpath(outDir, runName *"_GroupComparison.png" ))
	savefig(subgroupsCombinedPlot,joinpath(outDir, runName *"_SubGroupComparison.png" ))
	savefig(subgroupsCostCombinedPlot,joinpath(outDir, runName *"_SubGroupCostComparison.png" ))
end

# ╔═╡ 9bc0f934-430e-11eb-342d-7da1f88758b5
# #The original standalone plotting code

# foodsSubGroupPlot1 = @df odsg[odsg.servingSums .!= 0,:] bar( :foodSubGroup,:servingSums,legend=false,xlabel="Servings",orientation=:horizontal,title="Total diet cost \$" * c)

# foodsSubGroupCostPlot1 = @df odsg[odsg.servingSums .!= 0,:] bar( :foodSubGroup,:Cost,legend=false,xlabel="Cost",orientation=:horizontal,title="Total diet cost \$" * c)

# 	groupsPlot1 = @df fg bar( :foodGroup,:servingSums,legend=false,automargin=true,xlabel="Servings",orientation = :horizontal,yticks=(0.5:size(fg,1), fg.foodGroup),title="Baseline diet daily cost \$" * c)


# foodsSubGroupPlot2 = @df newodsg[newodsg.servingSums .!= 0,:] bar( :foodSubGroup,:servingSums,legend=false,xlabel="Servings",orientation=:horizontal,title="Total diet cost \$" * newc)

# foodsSubGroupCostPlot2 = @df newodsg[newodsg.servingSums .!= 0,:] bar( :foodSubGroup,:Cost,legend=false,xlabel="Cost",orientation=:horizontal,title="Total diet cost \$" * newc)


# groupsPlot2 = @df newfg bar( :foodGroup,:servingSums,legend=false,automargin=true,xlabel="Servings",orientation = :horizontal,yticks=(0.5:size(fg,1), fg.foodGroup),title="Scaled diet daily cost \$" * c,fill=:red)


# 	savefig(groupsPlot1,joinpath(outDir, "baseline_Groups.png"))
# 	savefig(foodsSubGroupPlot1,joinpath(outDir, "baseline_SubGroupServings.png"))
# 	savefig(foodsSubGroupCostPlot1,joinpath(outDir, "baseline_SubGroupCosts.png"))
# 	savefig(groupsPlot2,joinpath(outDir, runName * "_Groups.png"))
# 	savefig(foodsSubGroupPlot2,joinpath(outDir, runName * "_SubGroupServings.png"))
# 	savefig(foodsSubGroupCostPlot2,joinpath(outDir, runName * "_SubGroupCosts.png"))


# ╔═╡ Cell order:
# ╠═36bd7e34-0648-4719-8612-d275f91b6af7
# ╠═9451e15f-848c-4f6e-b66c-618c4e1f134a
# ╟─8171b692-3ab0-11eb-38d7-8b140e7b03dd
# ╠═4332e264-3a58-11eb-032d-f361adf14c3e
# ╟─01498128-3a62-11eb-1511-8d0bb7716a79
# ╟─e61c457c-83d9-11eb-3a30-97da23d252ad
# ╟─2defed70-3a62-11eb-09f6-43f4266cec68
# ╟─7652ac3c-3a63-11eb-11e3-3b46901568d6
# ╟─8b8988c2-82e4-11eb-39d3-9573fc078b05
# ╟─811c22fa-82e4-11eb-1753-8bfa8b4eb72d
# ╟─d538bb58-82e6-11eb-3bd3-dbc277cbdce8
# ╟─799ea3e4-8f7b-11eb-2fc8-d50ca20e4124
# ╟─1bb4846f-377c-43f0-841a-bc13daa94a3b
# ╟─a2c809c2-3bca-41e3-833a-ffd7438abf56
# ╟─c223ce40-43d0-11eb-20f8-79f9181f7d09
# ╟─1b09cbec-82e7-11eb-0d76-e76520a690f1
# ╟─22c77504-2d36-4398-95b9-7be619aa0525
# ╟─04ff380c-83b0-11eb-09cf-e7b7d482cb9c
# ╟─f2ff3774-83af-11eb-1638-b599eebaafb0
# ╟─1c889c64-83da-11eb-15b7-7fcd7bf7a98e
# ╟─d98a0e30-8d47-11eb-173e-1d1378892115
# ╟─3968203a-ed29-4a4c-b02f-a6b4eb2a667d
# ╟─15772110-8cff-11eb-0b09-8d0869183986
# ╟─db083cb6-43d0-11eb-3bb6-114b750ca17b
# ╟─51b39790-8f97-11eb-1114-c7c0854527ff
# ╟─2be2d2b3-1cbd-498e-ba21-8456cebed7ee
# ╟─dfbc49b0-aab9-4041-bea7-4da3d91b0261
# ╟─767859cc-891a-11eb-2d85-6fe878eeac9f
# ╟─75c26eb4-83bb-11eb-03df-9b5676f9be43
# ╟─82dfda0a-83b1-11eb-319a-3385adad41ca
# ╟─8ab4f6b6-83b1-11eb-2d8d-15a56d5e1374
# ╟─933519c2-83b1-11eb-14cc-f7e11f800af5
# ╟─2c7c833c-83b3-11eb-2ac1-05ec7d477519
# ╟─831978b4-83b3-11eb-1eaf-d346ae4503c0
# ╟─a89c6e60-9977-4e36-afa8-3dc999ea6afd
# ╟─9c0560b8-83b1-11eb-0ed6-852d676a6dc8
# ╟─c02e8a6a-43d2-11eb-1069-7d35890c9bb8
# ╟─243f99a2-83d6-11eb-094c-0757557186a2
# ╟─badc7782-838a-11eb-3a87-1d940979a158
# ╟─83c702c4-8387-11eb-27b0-8b80b54991fc
# ╟─f9ec5e98-3aaa-11eb-1ca6-81851af40f24
# ╟─48a88a48-82dd-11eb-3f7e-0da488f56434
# ╟─9bc0f934-430e-11eb-342d-7da1f88758b5
